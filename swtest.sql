-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2020 at 07:33 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_prop`
--

CREATE TABLE `book_prop` (
  `id` int(11) NOT NULL,
  `weight` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `book_prop`
--

INSERT INTO `book_prop` (`id`, `weight`) VALUES
(169, 740),
(172, 500);

-- --------------------------------------------------------

--
-- Table structure for table `dvd_prop`
--

CREATE TABLE `dvd_prop` (
  `id` int(11) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dvd_prop`
--

INSERT INTO `dvd_prop` (`id`, `size`) VALUES
(168, 22000),
(171, 700);

-- --------------------------------------------------------

--
-- Table structure for table `furniture_prop`
--

CREATE TABLE `furniture_prop` (
  `id` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `lenght` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `furniture_prop`
--

INSERT INTO `furniture_prop` (`id`, `height`, `width`, `lenght`) VALUES
(170, 112, 322, 123);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `type` varchar(50) NOT NULL,
  `reg_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type`, `reg_time`) VALUES
(168, 'KLR-322311-112', 'Back To The Future', 98.99, 'dvd', '2020-04-09 17:20:54'),
(169, 'VCDD-32C-43-WE', 'NASA - 1999 year', 99.99, 'book', '2020-04-09 17:22:18'),
(170, 'PWI-43FF211-1', 'Zebra', 73, 'furniture', '2020-04-09 17:23:02'),
(171, 'EEL-23-2332-2', 'UFO 2021', 22.99, 'dvd', '2020-04-09 17:27:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_prop`
--
ALTER TABLE `book_prop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dvd_prop`
--
ALTER TABLE `dvd_prop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furniture_prop`
--
ALTER TABLE `furniture_prop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
