<?php
    $dbUser = "root";
    $dbPass = ""; 
    $dbName = "swtest";

    try{
        $db = new PDO('mysql:host=localhost;dbname='. $dbName . ';charset-utf8', $dbUser, $dbPass);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo 'DB Connected';
    }

    catch(PDOException $error){
        $error->getMessage();
    }
?>