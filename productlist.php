<?php
    require_once('config.php');
    // Get data from DB tables
    $sth = $db->prepare("SELECT
                            products.*,
                            dvd_prop.size,
                            furniture_prop.height,
                            furniture_prop.width,
                            furniture_prop.lenght,
                            book_prop.weight
                            FROM 
                        products
                            LEFT JOIN dvd_prop on products.id = dvd_prop.id 
                            LEFT JOIN furniture_prop on products.id = furniture_prop.id 
                            LEFT JOIN book_prop on products.id = book_prop.id");
    $sth->execute();
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);

    // Submiting
    if(isset($_POST['submit'])){
        if(isset($_POST['ids'])){
            foreach($_POST['ids'] as $id){
                $sql = "DELETE FROM `products` WHERE id = :id";   
                $q = $db->prepare($sql);
                $q->execute(['id' => $id]); 
            }
            header("Location:/www/sw_mytest/productlist.php");
            exit();
        }
    }
?>

<html>
    <head>
        <!-- meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        
        <!-- My Css -->
        <link rel="stylesheet" type="text/css" href="liststyle.css">
        <title>Product List</title>
    </head>
    <body>
        <div class="container">
            <form action="" method="post" onsubmit="return confirm('Are you sure want to delete this product(s)?');">
                <h1 class="text-center tittle">Product list</h1>
                <hr>
                <div class="deletebtn-checkbox-linkbtn mb-3">
                    <p class="d-inline mr-3 pt-2 check-box">Check all <input type="checkbox" id="checkAll"></p>
                    <button type="submit" value="delete" name="submit" class="btn btn-default delete-btn">Delete</button>
                    <button type="button" class="btn btn-default ml-3 link-btn"><a href="http://localhost/www/sw_mytest/addproduct.php">ADD Product</a></button>
                </div>
                <div class="row justify-content-start">
                    <?php foreach($data as $row) { ?>
                        <div class="col-md-3 product-box">
                            <!-- Get Products with row -->
                            <div class="product-checkbox d-flex justify-content-end">
                                <input type="checkbox" class="checkProduct" 
                                value="<?php echo $row["id"];?>" name="ids[]">
                            </div>
                            <div class="product-basic-info">
                                <h5 class="text-center">SKU: <?php echo $row["sku"];?></h5>
                                <p class="text-center">Name: <?php echo $row["name"];?></p>
                                <p class="text-center">Price: <?php echo $row["price"];?>$</p>
                                <p class="text-center">Type: <?php echo $row["type"];?></p>
                            </div>
                            <div class="product-dinamic-info" id="product-dinamic-info">
                                <?php if($row["type"] == 'dvd') { ?>
                                    <p class="card-text text-center">Size: <?php echo $row["size"];?> MB</p> 
                                <?php }elseif($row["type"] == 'furniture') { ?>
                                    <div class="col-sm text-center">
                                        <p class=" d-inline card-text text-center">Size: <?php echo $row["height"];?></p> 
                                        <p class=" d-inline card-text text-center"> X <?php echo $row["width"];?></p> 
                                        <p class=" d-inline card-text text-center"> X <?php echo $row["lenght"];?></p> 
                                    </div>
                                <?php }elseif($row["type"] == 'book') { ?>
                                    <p class="card-text text-center"><?php echo $row["weight"];?> gr</p> 
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php require_once('scripts.php'); ?>
        <script>
            $(document).ready(function(){
                $("#checkAll").click(function(){
                    if($(this).is(":checked")){
                        $(".checkProduct").prop("checked", true);
                    }
                    else{
                        $(".checkProduct").prop("checked", false);
                    }
                });
            });
        </script>
    </body>
</html>
