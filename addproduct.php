<?php
    require_once('config.php');
    if(isset($_POST["submit"])){

        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $type = $_POST['type'];

        // Insert data in to DB PDO
        $sql = "INSERT INTO `products`(`sku`, `name`, `price`, `type`) VALUES (:sku,:name,:price,:type)";
        $sql = $db->prepare($sql);
        $sql->execute(array('sku' => $sku, 'name' => $name, 'price' => $price, 'type' => $type));
        $productId =  $db->lastInsertId();

        // Type validation and data insert in corresponding table 
        if($type == "dvd"){
            $size = $_POST['size'];
            $sql = "INSERT INTO `dvd_prop`(`id`, `size`) VALUES (:id,:size)";
            $sql = $db->prepare($sql);
            $sql->execute(array('id' => $productId, 'size' => $size));

        }elseif ($type == "furniture") {
            $height = $_POST['height'];
            $width = $_POST['width'];
            $lenght = $_POST['lenght'];
            $sql = "INSERT INTO `furniture_prop`(`id`, `height`, `width`, `lenght`) VALUES (:id,:height,:width,:lenght)";
            $sql = $db->prepare($sql);
            $sql->execute(array('id' => $productId, 'height' => $height, 'width' => $width, 'lenght' => $lenght));

        }elseif($type == "book") {
            $weight = $_POST['weight'];
            $sql = "INSERT INTO `book_prop`(`id`, `weight`) VALUES (:id,:weight)";
            $sql = $db->prepare($sql);
            $sql->execute(array('id' => $productId, 'weight' => $weight));
        }
        header('Location: http://localhost/www/sw_mytest/productlist.php');
        exit;
    }else{
?>
    <html>
        <head>
            <meta charset="utf-8">
            <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
            <!-- Bootstrap-->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

            <!-- My Css -->
            <link rel="stylesheet" type="text/css" href="addstyle.css">
            
            <title>Add Product</title>
        </head>

        <body>
            <div class="wrapper col-4">
                <form id="add-form" action="" method="post">
                    <div class="form-field">
                        <div class="input-center">
                            <label for="sku">Sku: </label>
                            <input type="text" id="sku" class="basic-info-input" name="sku" autocomplete="off"><br>
                            <span id="skuCheck" class="error"></span>
                        </div>
                    </div>

                    <div class="form-field">
                        <div class="input-center">
                            <label for="name">Name: </label>
                            <input type="text" id="name" class="basic-info-input" name="name" autocomplete="off"><br>
                            <span id="nameCheck" class="error"></span>
                        </div>
                    </div>

                    <div class="form-field">
                        <div class="input-center">
                            <label for="price">Price: </label>
                            <input type="text" id="price" class="basic-info-input" name="price" autocomplete="off"><br>
                            <span id="priceCheck" class="error"></span>
                        </div>
                    </div>

                   <div class="form-field dd-type">
                        <select id="type" name="type" class="select-style">
                            <option value="0">Choose type</option>
                            <option value="dvd">DVD-disc</option>
                            <option value="furniture">Furniture</option>
                            <option value="book">Book</option>
                        </select><br>
                   </div>

                   <span id="typeCheck" class="error-type"></span>
                   
                    <!-- Dynamical stuff -->

                   <div class="form-field">
                        <div id="dvd" class="this showable">
                            <div class="size-n-weight-input">
                                <input type="text" class="onlyDigits" name="size" placeholder="Size in MB">
                            </div>
                        </div>
                   </div>

                    <div class="form-field">
                        <div id="furniture" class="this showable">
                            <div class="alert"><p><b>Attension! Dimensions must be indicated in cm.</b><p></div>
                            <div class="alert mb-4"><p>Please provide dimensions in H x W x L format.</p></div>
                            <div class="height-width-lenght-inputs">
                                <input type="text" class="onlyDigits" name="height" placeholder="Height">
                                <input type="text" class="onlyDigits" name="width" placeholder="Width">
                                <input type="text" class="onlyDigits" name="lenght" placeholder="Lenght">
                            </div>
                        </div>
                    </div>

                    <div class="form-field">
                        <div id="book" class="this showable">
                            <div class="size-n-weight-input">
                                <input type="text" class="onlyDigits" name="weight" placeholder="Weight in g">
                            </div>
                        </div>
                   </div>
                   <!-- ENDs -->
                    <input type="submit" value="submit" name="submit" id="submit" class="btn btn-default btn-style">

                    <button type="button" class="btn btn-style mt-3"><a href="http://localhost/www/sw_mytest/productlist.php">Product list</a></button>

                </form>
            </div>

            <script>
                $("#type").on("change", function() {
                $(".showable").hide();
                $("#" + $(this).val()).show();
                })
            </script>

            <?php require_once('scripts.php'); ?>
           
            <!-- Jquery input validation pre submiting -->
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#skuCheck').hide();
                    $('#nameCheck').hide();
                    $('#priceCheck').hide();
                    $('#typeCheck').hide();

                    var sku_error = true;
                    var name_error = true;
                    var price_error = true;
                    var type_error = true;

                    $('#sku').keyup(function(){
                        skuCheck();
                    });

                    // SKU 
                    function skuCheck(){
                        var sku_val = $('#sku').val();
                        
                        if(sku_val.length == ''){
                            $('#skuCheck').show();
                            $('#skuCheck').html("Please Fill the Sku number");
                            $('#skuCheck').focus();
                            $('#skuCheck').css("color","red");

                            sku_error = false;
                            return false;
                        }else{
                            $('#skuCheck').hide();
                        }

                        if(sku_val.length < 3){
                            $('#skuCheck').show();
                            $('#skuCheck').html("Please Fill the Sku number (min. lenght 3 characters)");
                            $('#skuCheck').focus();
                            $('#skuCheck').css("color","red");

                            sku_error = false;
                            return false;
                        }else{
                            $('#skuCheck').hide();
                        }
                    }

                    // NAME
                    $('#name').keyup(function(){
                        nameCheck();
                    });

                    function nameCheck(){
                        var name_val = $('#name').val();
                        
                        if(name_val.length == ''){
                            $('#nameCheck').show();
                            $('#nameCheck').html("Please Fill the product Name. (Input is empty)");
                            $('#nameCheck').focus();
                            $('#nameCheck').css("color","red");

                            name_error = false;
                            return false;
                        }else{
                            $('#nameCheck').hide();
                        }

                        if(name_val.length < 2){
                            $('#nameCheck').show();
                            $('#nameCheck').html("Please Fill the product Name. (min. lenght 2 characters)");
                            $('#nameCheck').focus();
                            $('#nameCheck').css("color","red");

                            name_error = false;
                            return false;
                        }else{
                            $('#nameCheck').hide();
                        }
                    }

                    $('#price').keyup(function(){
                        priceCheck();
                    });

                    // PRICE
                    $('#price').keypress(function(e) {

                        if (e.which != 46 && (e.which < 47 || e.which > 59)){
                            e.preventDefault();
                        if ((e.which == 46) && ($(this).indexOf('.') != -1)) {
                            e.preventDefault();
                            }
                        }
                    });

                    function priceCheck(){
                        var price_val = $('#price').val();

                        if(price_val.length == ''){
                            $('#priceCheck').show();
                            $('#priceCheck').html("Please Fill price. (Input is empty)");
                            $('#priceCheck').focus();
                            $('#priceCheck').css("color","red");

                            price_error = false;
                            return false;
                        }else{
                            $('#priceCheck').hide();
                        }

                        if(price_val.length < 3){
                            $('#priceCheck').show();
                            $('#priceCheck').html("Please Fill price in format (x.xx) !!!");
                            $('#priceCheck').focus();
                            $('#priceCheck').css("color","red");

                            price_error = false;
                            return false;
                        }else{
                            $('#priceCheck').hide();
                        }
                    }

                    $( "#type" ).change(function() {

                        if($('#type').val() == 0){

                            $('#typeCheck').show();
                            $('#typeCheck').html("Please Fill the Type. (Input is empty)");
                            $('#typeCheck').focus();
                            $('#typeCheck').css("color","red");

                            type_error = false;
                            return false;
                        }else{
                            $('#typeCheck').hide();
                        }
                    });

                    // DVD MB
                    $('.onlyDigits').bind('keyup paste', function(){
                        this.value = this.value.replace(/[^0-9]/g, '');
                    });

                    // Submiting Validation
                    $('#submit').click(function(){

                        var sku_val_empty = $('#sku').val();

                        if(sku_val_empty == ''){
                            $('#skuCheck').show();
                            $('#skuCheck').html("Please Fill the sku. (Input is empty)");
                            $('#skuCheck').focus();
                            $('#skuCheck').css("color","red");

                            sku_error = false;
                            return false;
                        }else{
                            $('#skuCheck').hide();
                        }
                        
                        $('#sku').keyup(function(){
                            skuCheck();
                        });

                        var name_val_empty = $('#name').val();

                        if(name_val_empty == ''){
                            $('#nameCheck').show();
                            $('#nameCheck').html("Please Fill the Name. (Input is empty)");
                            $('#nameCheck').focus();
                            $('#nameCheck').css("color","red");

                            name_error = false;
                            return false;
                        }else{
                            $('#nameCheck').hide();
                        }

                        $('#name').keyup(function(){
                            nameCheck();
                        });

                        var price_val_empty = $('#price').val();

                        if(price_val_empty == ''){
                            $('#priceCheck').show();
                            $('#priceCheck').html("Please Fill the Price. (Input is empty)");
                            $('#priceCheck').focus();
                            $('#priceCheck').css("color","red");

                            price_error = false;
                            return false;
                        }else{
                            $('#priceCheck').hide();
                        }

                        $('#price').keyup(function(){
                            priceCheck();
                        });

                        var type_val = $('#type').val();

                        if(type_val == 0){
                            $('#typeCheck').show();
                            $('#typeCheck').html("Please choose the Type");
                            $('#typeCheck').focus();
                            $('#typeCheck').css("color","red");

                            type_error = false;
                            return false;
                        }else{
                            $('#typeCheck').hide();
                        }
                        
                        var sku_error = true;
                        var name_error = true;
                        var price_error = true;
                        var type_error = true;
            
                        skuCheck();
                        nameCheck();
                        priceCheck();

                        if((sku_error == true) && (name_error == true) && (price_error == true) && (type_error == true)){
                            return true;
                        }else{
                            return false;
                        }
                    });
                });
            </script>
            <div class="overlay"></div>
        </body>
    </html>
<?php } ?>
